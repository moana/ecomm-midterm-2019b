<?php
// including the database connection file
include_once("dbconn.php");

if(isset($_POST['update']))
{	

	$id = mysqli_real_escape_string($mysqli, $_POST['id']);
	
	$name = mysqli_real_escape_string($mysqli, $_POST['name']);
	$phoneNum = mysqli_real_escape_string($mysqli, $_POST['phoneNum']);
	$email = mysqli_real_escape_string($mysqli, $_POST['email']);	
	$companyName = mysqli_real_escape_string($mysqli, $_POST['companyName']);
	$address = mysqli_real_escape_string($mysqli, $_POST['address']);
	$website = mysqli_real_escape_string($mysqli, $_POST['website']);

	
	// checking empty fields
	if(empty($name) || empty($phoneNum) || empty($email)) {	
			
		if(empty($name)) {
			echo "<font color='red'>Name field is empty.</font><br/>";
		}
		
		if(empty($phoneNum)) {
			echo "<font color='red'>Age field is empty.</font><br/>";
		}
		
		if(empty($email)) {
			echo "<font color='red'>Email field is empty.</font><br/>";
		}

		if(empty($companyName)) {
			echo "<font color='red'>Company Name field is empty.</font><br/>";
		}

		if(empty($address)) {
			echo "<font color='red'>Address field is empty.</font><br/>";
		}
	} else {	
		//updating the table
		$result = mysqli_query($mysqli, "UPDATE card SET name='$name',phoneNum='$phoneNum',email='$email',companyName='$companyName',address='address', website='website'  WHERE id=$id");
		
		//redirectig to the display page. In our case, it is index.php
		header("Location: index.php");
	}
}
?>
<?php
//getting id from url
$id = $_GET['id'];

//selecting data associated with this particular id
$result = mysqli_query($mysqli, "SELECT * FROM card WHERE id=$id");

while($res = mysqli_fetch_array($result))
{
	$name = $res['name'];
	$phoneNum = $res['phoneNum'];
	$email = $res['email'];
	$companyName = $res['companyName'];
	$address = $res['address'];
	$website = $res['website'];
}
?>
<html>
<head>	
	<title>Edit Data</title>
</head>

<body>
	<a href="index.php">Home</a>
	<br/><br/>
	
	<form name="form1" method="post" action="edit.php">
		<table border="0">
			<tr> 
				<td>Name</td>
				<td><input type="text" name="name" value="<?php echo $name;?>"></td>
			</tr>
			<tr> 
				<td>Age</td>
				<td><input type="text" name="phoneNum" value="<?php echo $phoneNum;?>"></td>
			</tr>
			<tr> 
				<td>Email</td>
				<td><input type="text" name="email" value="<?php echo $email;?>"></td>
			</tr>
			<tr> 
				<td>Company Name</td>
				<td><input type="text" name="companyName" value="<?php echo $companyName;?>"></td>
			</tr>
			<tr> 
				<td>Company Address</td>
				<td><input type="text" name="address" value="<?php echo $address;?>"></td>
			</tr>
			<tr> 
				<td>Website</td>
				<td><input type="text" name="website" value="<?php echo $website;?>"></td>
			</tr>
			<tr>
				<td><input type="hidden" name="id" value=<?php echo $_GET['id'];?>></td>
				<td><input type="submit" name="update" value="Update"></td>
			</tr>
		</table>
	</form>
</body>
</html>
