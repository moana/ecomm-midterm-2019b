<html>
<head>
	<title>Add Data</title>
</head>

<body>
<?php
//including the database connection file
include_once("dbconn.php");

if(isset($_POST['Submit'])) {	
	$name = $_POST['name'];
	$phoneNum = mysqli_real_escape_string($mysqli, $_POST['phoneNum']);
	$email = mysqli_real_escape_string($mysqli, $_POST['email']);
	$companyName = mysqli_real_escape_string($mysqli, $_POST['companyName']);
	$address = mysqli_real_escape_string($mysqli, $_POST['address']);
    $website = mysqli_real_escape_string($mysqli, $_POST['website']);	
		
	// checking empty fields
	if(empty($name) || empty($phoneNum) || empty($email)) {
				
		if(empty($name)) {
			echo "<font color='red'>Name field is empty.</font><br/>";
		}
		
		if(empty($phoneNum)) {
			echo "<font color='red'>Age field is empty.</font><br/>";
		}
		
		if(empty($email)) {
			echo "<font color='red'>Email field is empty.</font><br/>";
		}

		if(empty($companyName)) {
			echo "<font color='red'>Company Name field is empty.</font><br/>";
		}

		if(empty($address)) {
			echo "<font color='red'>Address field is empty.</font><br/>";
		}

		echo "<br/><a href='add.html'>Go Back</a>";
	} else { 
		$result = mysqli_query($mysqli, "INSERT INTO card(name,phoneNum,email,companyName,address,website) VALUES('$name','$phoneNum','$email','$companyName','$address', '$website')");
		
		//display success message
		echo "<font color='green'>Data added successfully.";
		echo "<br/><a href='index.php'>View Result</a>";
	}
}
?>
</body>
</html>
